# output "do_cluster" {
#   sensitive = true
#   value = {
#     host  = digitalocean_kubernetes_cluster.cluster.endpoint
#     ip    = digitalocean_kubernetes_cluster.cluster.ipv4_address
#     kube  = digitalocean_kubernetes_cluster.cluster.kube_config[0].host
#     count = digitalocean_kubernetes_cluster.cluster.node_pool
#   }
# }

output "do_cluster" {
  value = data.digitalocean_kubernetes_cluster.example.id
}
